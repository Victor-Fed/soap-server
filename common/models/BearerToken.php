<?php

namespace common\models;


use yii\db\ActiveRecord;

class BearerToken  extends ActiveRecord
{


    public static function tableName()
    {
        return '{{%bearer_token}}';
    }


    public function rules()
    {
        return [
            [['user_id', 'id'], 'integer'],
            [['token'], 'string'],
            [['date_create'], 'datetime'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'token' => 'Token',
        ];
    }
}
