<?php
namespace common\constants;

use frontend\models\Calculate;
use Yii;

class Soap {

    const CLASS_CALCULATE = Calculate::class;
    const PATH_WSDL_CALCULATE = '@app/wsdl/current/calculate.wsdl';
    const PATH_WSDL_TEMPLATE = '@app/wsdl/template/calculate.wsdl';
    const NS_SOAP= 'http://schemas.xmlsoap.org/wsdl/soap/';
}
