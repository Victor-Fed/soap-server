## SOAP-сервер на yii2
### Описание сервиса
Тестовый пример реализации soap-сервера
##### Описание методов
Сервис имеет один метод ```calculate```
На вход принимаются следующие параметры: city, name, date, comment, phone, email
Первые три параметра являются обязательными
Возвращаемые значения: price, info, error(в случае ошибки)
### Локальная установка
#### Системные требования:

 -  docker >= 18.0 (install: curl -fsSL get.docker.com | sudo sh)
 -  docker-compose >= 3 (installing manual)
 - git
 - composer
 
#### Установка:
Выполните следующие команды:
 
```
 $ git clone https://Victor-Fed@bitbucket.org/Victor-Fed/soap-server.git
 $ cd soap-server
 $ docker-compose up -d
 $ docker-compose  exec  frontend composer install
 $ docker-compose  exec  frontend php ./init
```
 Далее укажите в файле common\config\main-local.php конфиги для db(указаны в docker-compose.yml), пример:
```
 'db' => [
             'class' => 'yii\db\Connection',
             'dsn' => 'mysql:host=mysql;dbname=yii2advanced',
             'username' => 'yii2advanced',
             'password' => 'secret',
             'charset' => 'utf8',
         ],
 )
```
Затем выполните следующие команды:
```
$ docker-compose  exec  frontend php ./yii migrate 
$ docker-compose  exec  frontend php ./yii seed
```
Готово!
 
Теперь проект доступен  по [localhost:20080](http://localhost:20080); тестовый пользователь (login:test, password:123456)

