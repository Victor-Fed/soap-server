<?php
namespace frontend\controllers;

use common\constants\Soap;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\filters\auth\HttpBearerAuth;

/**
 * Soap controller
 */
class SoapController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'only'=>['index'],
            'class' => HttpBearerAuth::class,
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'index'  => ['post'],
                'wsdl'  => ['get'],
            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'text/xml');
        return parent::beforeAction($action);
    }

    /**
     * Returns calculateResponse in format XML
     * @return string
     * @soap
     * @throws HttpException
     */
    public function actionIndex()
    {

        $wsdl = Yii::getAlias(Soap::PATH_WSDL_CALCULATE);
        try {
            if(!file_exists($wsdl)){
                $this->genWsdl();
            }
            $server = new \SoapServer($wsdl,[
                'cache_wsdl' => WSDL_CACHE_NONE
            ]);

            $server->setClass(Soap::CLASS_CALCULATE);
            ob_start();
            $server->handle();
            return ob_get_clean();
        }
        catch (\Exception $e){
            Yii::$app->response->format = Response::FORMAT_XML;
            throw new HttpException(500, $e->getMessage());
        }

    }

    /**
     * Return wsdl
     * @return string
     * @throws HttpException
     */
    public function actionWsdl()
    {

        try {
            $wsdlPath = Yii::getAlias(Soap::PATH_WSDL_CALCULATE);
            if(!file_exists($wsdlPath)){
                $wsdl = $this->genWsdl();
            }
            else {
                $wsdl =  file_get_contents($wsdlPath);
            }

            return $wsdl;
        }
        catch (\Exception $e){
            throw new HttpException(500, $e->getMessage());
        }
    }


    /**
     * Return xml
     * @return string
     * @throws HttpException
     */
    private function genWsdl()
    {
        try {
            $doc = new \DOMDocument('1.0','utf-8');
            $doc->load(Yii::getAlias(Soap::PATH_WSDL_TEMPLATE));
            $operation = $doc->getElementsByTagNameNS(Soap::NS_SOAP,'operation');
            $address = $doc->getElementsByTagNameNS(Soap::NS_SOAP,'address');
            if($operation->length < 1 || $address->length < 1)
                throw new HttpException(400, "В wsdl не найдено поле operation|address");
            else {
                $doc->getElementsByTagNameNS(Soap::NS_SOAP,'operation')->item(0)->setAttribute('soapAction',Url::base('http').'/soap');
                $doc->getElementsByTagNameNS(Soap::NS_SOAP,'address')->item(0)->setAttribute('location',Url::base('http').'/soap');
            }

        }
        catch (\Exception $e){
            throw new HttpException(500, $e->getMessage());
        }
        $xml = $doc->saveXML();
        if(file_put_contents(Yii::getAlias(Soap::PATH_WSDL_CALCULATE), $xml))
            return $doc->saveXML();
        else
            throw new HttpException('500', 'Произошла ошибка при сохранении wsdl');
    }
}
