<?php


namespace frontend\models;

use yii\base\Model;

class Calculate extends Model
{
    public $city;
    public $name;
    public $date;
    public $comment;
    public $phone;
    public $email;

    private $quote = [
        'Есть преступления хуже, чем сжигать книги. Например — не читать их',
        'Решения, принятые глухой ночью, обычно теряют силу при свете дня',
        'Некоторые, обогнав всех, оказываются в пустыне',
        'Наилучший выход из душевных затруднений — выход в деятельную жизнь',
        'Хотя и не делаешь ничего плохого, иногда бывает важно извиниться',
        'Не доверяй человеку в том, что выходит за рамки его собственных интересов',
        'Человек в гневе более предсказуем',
        'Чем тише омут, тем профессиональней в нём черти',
        'Чудо противоречит не природе, а нашим знаниям о ней',
        'Инвестиции в бардак - увеличивают бардак',
        'Наука не занимается мнениями, наука занимается сокращением количества интерпретаций вокруг определённого факта',
    ];
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city', 'name', 'date'], 'required'],
            [['date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['date'], 'validateDate'],
            [['comment','city', 'name', 'date','phone','email'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'city' => 'Город',
            'name' => 'Имя',
            'date' => 'Дата',
            'comment' => 'Комментарий',
            'test1' => 'Телефон',
            'test2' => 'Почта',
        ];
    }


    public function calculate($data)
    {
        $this->load(get_object_vars($data),'');
        if ($this->validate()) {

            return [
                'price' => rand(10,1000),
                'info' => $this->randomText()
                ];
        }
        return ['error'=>serialize($this->errors)];
    }

    public function validateDate($attribute, $params)
    {
            if (strtotime($this->date) < strtotime(date('Y-m-d'))) {
                $this->addError($attribute, 'Некорректная дата');
            }
    }

    private function randomText()
    {
        return $this->quote[rand(0, count($this->quote) - 1)];
    }
}
