<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Генерация токена';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Полученный токен требуется передать в загловок Authorization, при запросе </p>
    <p>Пример: <code>Bearer bQZ54uJ6qjDQATwX_GMphdV0OUKPO71z</code></p>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                    'id' => 'token-form',
                    'action' => 'get-token'
            ]); ?>
            <div class="form-group">
                <?= Html::submitButton('Сгенерировать новый токен', ['class' => 'btn btn-primary', 'name' => 'token-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <p class="result"></p>
        </div>
    </div>
    <?php
    $js = <<<JS
    $('form').on('beforeSubmit', function(){
    let data = $(this).serialize();
    $.ajax({
    url: $(this).attr('action'),
    type: 'GET',
    data: data,
    success: function(res){
        console.log(res);
        $('.result').text(res)
    },
    error: function(){
        alert('Ошибка генерации');
    }
    });
    return false;
    });
JS;

    $this->registerJs($js);
    ?>

</div>
