<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\helpers\Url; ?>
<div class="site-index">
    <p>Для запроса требуется получить уникальный токен</p>
    <p>Алрес сервиса для запроса (метод POST): <strong><?=Url::base('http').'/soap' ?> </strong></p>
    <p><a href="<?=Url::base('http').'/soap/wsdl' ?>">Wsdl сервиса</a></p>
    <p>Пример xml для запроса</p>
    <code>&lt;?xml version="1.0" encoding="UTF-8"?&gt;<br>
        &lt;SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="RandomPrice"&gt;
        &lt;SOAP-ENV:Body&gt;
        &lt;ns1:calculate&gt;
        &lt;data&gt;
        &lt;name&gt;test 2&lt;/name&gt;
        &lt;city&gt;test 2&lt;/city&gt;
        &lt;date&gt;2021-11-11&lt;/date&gt;
        &lt;/data&gt;
        &lt;/ns1:calculate&gt;
        &lt;/SOAP-ENV:Body&gt;
        &lt;/SOAP-ENV:Envelope&gt;</code>
</div>
