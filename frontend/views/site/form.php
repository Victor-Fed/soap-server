<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Калькулятор';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                    'id' => 'calcute-form',
                    'action' => 'calcute',
            ]); ?>

            <?= $form->field($model, 'city')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'name')->textInput() ?>
            <?= $form->field($model, 'date')->textInput(['value' => date('Y-m-d')]) ?>

            <div class="form-group">
                <?= Html::submitButton('Рассчитать', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <p class="result" id="success" style="border: #19ff32 solid 1px; display: none"></p>
            <p class="result" id="error" style="border: #f00 solid 1px; display: none"></p>
        </div>
        <style>
            .result {
                border-radius:5px;
                padding: 10px;
            }
        </style>
    </div>

    <?php
    $js = <<<JS
         $('form').on('beforeSubmit', function(){
         let data = $(this).serialize();
         $.ajax({
         url: $(this).attr('action'),
         type: 'POST',
         data: data,
         success: function(res){
            $('.result').css( 'display', 'none' )
            $('#success').css('display', 'block').text(res.info+' Сумма: '+res.price)
         },
         error: function(xhr, status, error){
             let message = JSON.parse(xhr.responseText).message;
             $('.result').css( 'display', 'none')
             $('#error').css('display', 'block').text(message)
         }
         });
         return false;
         });
JS;

    $this->registerJs($js);
    ?>
</div>
