<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bearer_token}}`.
 */
class m200226_013528_create_bearer_token_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bearer_token}}', [
            'id' => $this->primaryKey(),
            'token' => $this->string(),
            'user_id' => $this->integer(),
            'date_create' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bearer_token}}');
    }
}
