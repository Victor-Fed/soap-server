<?php
namespace console\controllers;

use common\models\User;
use frontend\models\Author;
use frontend\models\Journal;
use yii\console\Controller;
use yii\db\Exception;

class SeedController extends Controller
{
    public $user = [
                'id'=>1,
                'username' => 'test',
                'email' => 'test@email.com',
                'password' => '123456',
                'status' => User::STATUS_ACTIVE
           ];


    /**
     * @return bool
     */
    public function actionIndex()
    {
        $username = $this->user['username'];
        $password = $this->user['password'];
        $email = $this->user['email'];
        $status = $this->user['status'];
        try {
            $user = new User();
            $user->id = $this->user['id'];
            $user->username = $username;
            $user->email = $email;
            $user->status = $status;
            $user->setPassword($password);
            $user->generateAuthKey();
            $user->save();
        }
        catch (\Exception $e){
            echo $e->getMessage().PHP_EOL;
            return false;
        }
        echo 'Тестовый пользователь успешно   добавлен'.PHP_EOL.'username: '.$username.' password:'.$password;
        return true;

    }

   
}